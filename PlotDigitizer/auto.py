import cv2
import numpy as np

from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtWidgets import QMessageBox
import matplotlib.pyplot as plt
import numpy as np
from numpy.linalg import inv
import sys
import csv
import os

def find_affine_transform(src_points, dst_points):
    src_matrix = np.vstack([np.transpose(src_points), np.ones((1, 3))])
    dst_matrix = np.vstack([np.transpose(dst_points), np.ones((1, 3))])

    # Находим матрицу преобразования из исходных в целевые координаты
    transform_matrix = np.dot(dst_matrix, inv(src_matrix))

    return transform_matrix


class ImageScroller(QtWidgets.QWidget):
    def __init__(self, imageName, outputName, x1init, y1init, x2init, y2init, x3init, y3init, iter, MinArea, MaxArea):
        self.point_number = 1
        self.imageName = imageName
        self.outputName = outputName
        self.x1init = x1init
        self.y1init = y1init
        self.x2init = x2init
        self.y2init = y2init
        self.x3init = x3init
        self.y3init = y3init
        self.iter = iter
        self.MinArea = MinArea
        self.MaxArea = MaxArea
        self.chosen_points = []
        QtWidgets.QWidget.__init__(self)
        self._image = QtGui.QPixmap(imageName)
        self.setWindowTitle('Pick (x1,y1)')

    def paintEvent(self, paint_event):
        painter = QtGui.QPainter(self)
        painter.drawPixmap(self.rect(), self._image)
        pen = QtGui.QPen(QtCore.Qt.red)
        pen.setWidth(10)
        painter.setPen(pen)
        painter.setRenderHint(QtGui.QPainter.Antialiasing, True)
        for pos in self.chosen_points:
            painter.drawPoint(pos)

    def mouseReleaseEvent(self, cursor_event):
        if self.point_number == 1:
                self.x0 = cursor_event.pos().x
                self.y0 = cursor_event.pos().y
                self.chosen_points.append(cursor_event.pos())
                self.setWindowTitle('Pick (x2,y2)')
                self.point_number += 1
        elif self.point_number == 2:
                self.x1 = cursor_event.pos().x
                self.y1 = cursor_event.pos().y
                self.chosen_points.append(cursor_event.pos())
                self.setWindowTitle('Pick (x3,y3)')
                self.point_number += 1
        elif self.point_number == 3:
                self.x2 = cursor_event.pos().x
                self.y2 = cursor_event.pos().y
                self.chosen_points.append(cursor_event.pos())
                self.point_number += 1
                self.close()
        self.update()


    def closeEvent(self, event):
        if self.point_number < 4:
                msg = QMessageBox(self)
                msg.setWindowTitle("Error!")
                msg.setText("Minimum 3 points please!")
                msg.setIcon(QMessageBox.Warning)
                msg.exec_()
                event.ignore()
        else:
                # Load image, grayscale, Otsu's threshold
                image = cv2.imread(self.imageName)
                gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]

                # Filter out large non-connecting objects
                cnts = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                cnts = cnts[0] if len(cnts) == 2 else cnts[1]
                for c in cnts:
                    area = cv2.contourArea(c)
                if area < 500:
                    cv2.drawContours(thresh,[c],0,0,-1)

                # Morph open using elliptical shaped kernel
                kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3,3))
                opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=self.iter)

                xlist = []
                ylist = []
                xlist1 = []
                ylist1 = []

                # Find circles
                cnts = cv2.findContours(opening, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                cnts = cnts[0] if len(cnts) == 2 else cnts[1]
                for c in cnts:
                    area = cv2.contourArea(c)
                    if area > self.MinArea and area < self.MaxArea:
                        ((x, y), r) = cv2.minEnclosingCircle(c)
                        xlist1.append(x)
                        ylist1.append(y)
                        cv2.circle(image, (int(x), int(y)), int(r), (36, 255, 12), 2)

                cv2.imshow('gray', gray)
                cv2.imshow('thresh', thresh)
                cv2.imshow('opening', opening)
                cv2.imshow('image', image)
                cv2.waitKey()

                #resizing

                x0 = self.chosen_points[0].x()
                y0 = self.chosen_points[0].y()

                A = find_affine_transform([[self.chosen_points[0].x(), self.chosen_points[0].y()], [self.chosen_points[1].x(), self.chosen_points[1].y()], [self.chosen_points[2].x(), self.chosen_points[2].y()]], [[self.x1init, self.y1init], [self.x2init, self.y2init], [self.x3init, self.y3init]])
                for i in range(len(xlist1)):
                        xycoord=np.dot(A, [[xlist1[i]], [ylist1[i]], [1]])
                        xlist.append(xycoord[0])
                        ylist.append(xycoord[1])

                coord = dict()

                header = ['X','Y']
                coordFile = open(self.outputName + '.csv', 'w+')
                writer = csv.DictWriter(coordFile, fieldnames = header)
                writer.writeheader()
                for i in range(len(xlist)):
                    coord['X'] = xlist[i]
                    coord['Y'] = ylist[i]
                    writer.writerow(coord)
                coordFile.close()
                self.close()

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    print(sys.argv[0])
    w = ImageScroller(sys.argv[1], sys.argv[2], float(sys.argv[3]), float(sys.argv[4]), float(sys.argv[5]), float(sys.argv[6]), float(sys.argv[7]), float(sys.argv[8]), int(sys.argv[9]), int(sys.argv[10]),int(sys.argv[11]))
    w.show()
    sys.exit(app.exec_())


