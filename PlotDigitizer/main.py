# This Python file uses the following encoding: utf-8
import sys
from PySide2.QtWidgets import QApplication

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import *
import os


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(567, 460)

        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")

        self.MainTab = QtWidgets.QStackedWidget(self.centralwidget)
        self.MainTab.setGeometry(QtCore.QRect(10, 10, 771, 551))
        self.MainTab.setObjectName("MainTab")

        self.MainTabPage1 = QtWidgets.QWidget()
        self.MainTabPage1.setObjectName("MainTabPage1")

        self.SelectImage = QtWidgets.QLabel(self.MainTabPage1)
        self.SelectImage.setGeometry(QtCore.QRect(20, 0, 91, 17))
        self.SelectImage.setObjectName("SelectImage")

        self.imageSearch = QtWidgets.QLineEdit(self.MainTabPage1)
        self.imageSearch.setGeometry(QtCore.QRect(20, 20, 471, 25))
        self.imageSearch.setObjectName("imageSearch")

        def browsefiles():
            imname = QFileDialog.getOpenFileName(self,'Open launch file',"../",'Image(*.png *.jpg)')
            self.imageSearch.setText(imname[0])

        self.StartSearch = QtWidgets.QPushButton(self.MainTabPage1)
        self.StartSearch.setGeometry(QtCore.QRect(500, 20, 31, 21))
        self.StartSearch.clicked.connect(browsefiles)
        self.StartSearch.setObjectName("StartSearch")

        def browsefolder():
            dname = QFileDialog.getExistingDirectory(self,'Open launch file',"../", QFileDialog.ShowDirsOnly)
            self.OutputSearch_2.setText(dname)

        self.SelectFolder = QtWidgets.QLabel(self.MainTabPage1)
        self.SelectFolder.setGeometry(QtCore.QRect(20, 50, 151, 17))
        self.SelectFolder.setObjectName("SelectFolder")

        self.OutputSearch_2 = QtWidgets.QLineEdit(self.MainTabPage1)
        self.OutputSearch_2.setGeometry(QtCore.QRect(20, 70, 471, 25))
        self.OutputSearch_2.setObjectName("OutputSearch_2")

        self.StartSearch_2 = QtWidgets.QPushButton(self.MainTabPage1)
        self.StartSearch_2.setGeometry(QtCore.QRect(500, 70, 31, 21))
        self.StartSearch_2.clicked.connect(browsefolder)
        self.StartSearch_2.setObjectName("StartSearch_2")

        self.OutputNameL = QtWidgets.QLabel(self.MainTabPage1)
        self.OutputNameL.setGeometry(QtCore.QRect(20, 100, 131, 17))
        self.OutputNameL.setObjectName("OutputNameL")

        self.OutputNameStr_3 = QtWidgets.QLineEdit(self.MainTabPage1)
        self.OutputNameStr_3.setGeometry(QtCore.QRect(20, 120, 471, 25))
        self.OutputNameStr_3.setObjectName("OutputNameStr_3")

        self.FSPL = QtWidgets.QLabel(self.MainTabPage1)
        self.FSPL.setGeometry(QtCore.QRect(20, 160, 141, 17))
        self.FSPL.setObjectName("FSPL")

        self.X1L = QtWidgets.QLabel(self.MainTabPage1)
        self.X1L.setGeometry(QtCore.QRect(20, 190, 21, 17))
        self.X1L.setObjectName("X1L")

        self.Y1L = QtWidgets.QLabel(self.MainTabPage1)
        self.Y1L.setGeometry(QtCore.QRect(20, 220, 21, 17))
        self.Y1L.setObjectName("Y1L")

        self.X1LE = QtWidgets.QLineEdit(self.MainTabPage1)
        self.X1LE.setGeometry(QtCore.QRect(50, 190, 51, 21))
        self.X1LE.setText('0')
        self.X1LE.setObjectName("X1LE")

        self.Y1LE = QtWidgets.QLineEdit(self.MainTabPage1)
        self.Y1LE.setGeometry(QtCore.QRect(50, 220, 51, 21))
        self.Y1LE.setText('0')
        self.Y1LE.setObjectName("Y1LE")

        self.comboBox = QtWidgets.QComboBox(self.MainTabPage1)
        self.comboBox.setGeometry(QtCore.QRect(30, 260, 211, 31))
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")

        self.FADL = QtWidgets.QLabel(self.MainTabPage1)
        self.FADL.setGeometry(QtCore.QRect(20, 300, 161, 17))
        self.FADL.setObjectName("FADL")

        self.ItL = QtWidgets.QLabel(self.MainTabPage1)
        self.ItL.setGeometry(QtCore.QRect(20, 330, 81, 21))
        self.ItL.setObjectName("ItL")

        self.Iter_SB = QtWidgets.QSpinBox(self.MainTabPage1)
        self.Iter_SB.setGeometry(QtCore.QRect(100, 330, 48, 26))
        self.Iter_SB.setValue(4)
        self.Iter_SB.setMinimum(1)
        self.Iter_SB.setObjectName("Iter_SB")

        self.X2L = QtWidgets.QLabel(self.MainTabPage1)
        self.X2L.setGeometry(QtCore.QRect(120, 190, 21, 17))
        self.X2L.setObjectName("X2L")

        self.Y2LE = QtWidgets.QLineEdit(self.MainTabPage1)
        self.Y2LE.setGeometry(QtCore.QRect(150, 220, 51, 21))
        self.Y2LE.setText('0')
        self.Y2LE.setObjectName("Y2LE")

        self.Y2L = QtWidgets.QLabel(self.MainTabPage1)
        self.Y2L.setGeometry(QtCore.QRect(120, 220, 21, 17))
        self.Y2L.setObjectName("Y2L")

        self.X2LE = QtWidgets.QLineEdit(self.MainTabPage1)
        self.X2LE.setGeometry(QtCore.QRect(150, 190, 51, 21))
        self.X2LE.setObjectName("X2LE")

        self.X3L = QtWidgets.QLabel(self.MainTabPage1)
        self.X3L.setGeometry(QtCore.QRect(220, 190, 21, 17))
        self.X3L.setObjectName("X3L")

        self.Y3LE = QtWidgets.QLineEdit(self.MainTabPage1)
        self.Y3LE.setGeometry(QtCore.QRect(250, 220, 51, 21))
        self.Y3LE.setObjectName("Y3LE")

        self.Y3L = QtWidgets.QLabel(self.MainTabPage1)
        self.Y3L.setGeometry(QtCore.QRect(220, 220, 21, 17))
        self.Y3L.setObjectName("Y3L")

        self.X3LE = QtWidgets.QLineEdit(self.MainTabPage1)
        self.X3LE.setGeometry(QtCore.QRect(250, 190, 51, 21))
        self.X3LE.setText('0')
        self.X3LE.setObjectName("X3LE")

        self.MinMAL = QtWidgets.QLabel(self.MainTabPage1)
        self.MinMAL.setGeometry(QtCore.QRect(20, 370, 111, 17))
        self.MinMAL.setObjectName("MinMAL")

        self.MaxMAL = QtWidgets.QLabel(self.MainTabPage1)
        self.MaxMAL.setGeometry(QtCore.QRect(240, 370, 111, 17))
        self.MaxMAL.setObjectName("MaxMAL")

        self.MinAreaLE = QtWidgets.QLineEdit(self.MainTabPage1)
        self.MinAreaLE.setGeometry(QtCore.QRect(130, 370, 91, 21))
        self.MinAreaLE.setText('20')
        self.MinAreaLE.setObjectName("MinAreaLE")

        self.MaxAreaLE = QtWidgets.QLineEdit(self.MainTabPage1)
        self.MaxAreaLE.setGeometry(QtCore.QRect(350, 370, 91, 21))
        self.MaxAreaLE.setText('200')
        self.MaxAreaLE.setObjectName("MaxAreaLE")

        self.MainTab.addWidget(self.MainTabPage1)
        self.widget = QtWidgets.QWidget()
        self.widget.setObjectName("widget")
        self.MainTab.addWidget(self.widget)
        MainWindow.setCentralWidget(self.centralwidget)

        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 567, 22))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)


        def digitize():
            imageName = self.imageSearch.text()
            outputFolder = self.OutputSearch_2.text()
            outputName = self.OutputNameStr_3.text()
            if imageName == '':
                msg = QMessageBox(self)
                msg.setWindowTitle("Error!")
                msg.setText("Choose an image, please!")
                msg.setIcon(QMessageBox.Warning)
                msg.exec_()
            else:
                if outputFolder == '':
                    msg2 = QMessageBox(self)
                    msg2.setWindowTitle("Error!")
                    msg2.setText("Choose output folder, please!")
                    msg2.setIcon(QMessageBox.Warning)
                    msg2.exec_()
                else:
                    if outputName == '':
                        msg2 = QMessageBox(self)
                        msg2.setWindowTitle("Error!")
                        msg2.setText("Enter output name, please!")
                        msg2.setIcon(QMessageBox.Warning)
                        msg2.exec_()
                    else:
                        x1init = self.X1LE.text()
                        y1init = self.Y1LE.text()
                        x2init = self.X2LE.text()
                        y2init = self.Y2LE.text()
                        x3init = self.X3LE.text()
                        y3init = self.Y3LE.text()
                        if ((x1init  == '') or (y1init == '') or (x2init  == '') or (y2init == '') or (x3init  == '') or (y3init == '')):
                            msg2 = QMessageBox(self)
                            msg2.setWindowTitle("Error!")
                            msg2.setText("Input Points for Scaling, please!")
                            msg2.setIcon(QMessageBox.Warning)
                            msg2.exec_()
                        else:
                            if self.comboBox.currentText() == 'Manual':
                                os.system('python manual.py '+ imageName + ' ' + outputFolder + '/' + outputName +' '+x1init+' '+ y1init+' '+x2init+' '+ y2init+' '+x3init+' '+ y3init)
                            else:
                                if ((self.MinAreaLE.text() == '') or (self.MaxAreaLE.text()=='')):
                                    msg2 = QMessageBox(self)
                                    msg2.setWindowTitle("Error!")
                                    msg2.setText("Max and Min areas of the markers, please!")
                                    msg2.setIcon(QMessageBox.Warning)
                                    msg2.exec_()
                                else:
                                    os.system('python auto.py '+ imageName + ' ' + outputFolder + '/' + outputName +' '+x1init+' '+ y1init+' '+x2init+' '+ y2init+' '+x3init+' '+ y3init + ' ' + str(self.Iter_SB.value())+ ' '+self.MinAreaLE.text()+' ' + self.MaxAreaLE.text())


        self.Digitize = QtWidgets.QPushButton(self.MainTabPage1)
        self.Digitize.setGeometry(QtCore.QRect(310, 260, 191, 31))
        self.Digitize.clicked.connect(digitize)
        self.Digitize.setObjectName("Digitize")

        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "PlotDigitizer"))
        self.SelectImage.setText(_translate("MainWindow", "Select Image"))
        self.StartSearch.setText(_translate("MainWindow", "..."))
        self.SelectFolder.setText(_translate("MainWindow", "Select Output Folder"))
        self.StartSearch_2.setText(_translate("MainWindow", "..."))
        self.OutputNameL.setText(_translate("MainWindow", "Name Output File"))
        self.Digitize.setText(_translate("MainWindow", "Digitize"))
        self.FSPL.setText(_translate("MainWindow", "For Scaling Plots"))
        self.X1L.setText(_translate("MainWindow", "X1"))
        self.Y1L.setText(_translate("MainWindow", "Y1"))
        self.comboBox.setItemText(0, _translate("MainWindow", "Manual"))
        self.comboBox.setItemText(1, _translate("MainWindow", "Auto"))
        self.FADL.setText(_translate("MainWindow", "For Auto Detection"))
        self.ItL.setText(_translate("MainWindow", "Iterations"))
        self.X2L.setText(_translate("MainWindow", "X2"))
        self.Y2L.setText(_translate("MainWindow", "Y2"))
        self.X3L.setText(_translate("MainWindow", "X3"))
        self.Y3L.setText(_translate("MainWindow", "Y3"))
        self.MinMAL.setText(_translate("MainWindow", "MIn Mark Area"))
        self.MaxMAL.setText(_translate("MainWindow", "Max Mark Area"))

class Window(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)



if __name__ == "__main__":
    app = QApplication([])
    window = Window()
    window.show()
    sys.exit(app.exec_())
